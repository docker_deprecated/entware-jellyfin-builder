# entware-jellyfin-builder

#### [entware-x64-jellyfin-builder](https://hub.docker.com/r/forumi0721entwarex64build/entware-x64-jellyfin-builder/)
![Docker Image Version (tag latest semver)](https://img.shields.io/docker/v/forumi0721entwarex64build/entware-x64-jellyfin-builder/latest)
![Docker Image Size (tag)](https://img.shields.io/docker/image-size/forumi0721entwarex64build/entware-x64-jellyfin-builder/latest)
![MicroBadger Layers (tag)](https://img.shields.io/microbadger/layers/forumi0721entwarex64build/entware-x64-jellyfin-builder/latest)
![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721entwarex64build/entware-x64-jellyfin-builder)
![Docker Stars](https://img.shields.io/docker/stars/forumi0721entwarex64build/entware-x64-jellyfin-builder)
#### [entware-aarch64-jellyfin-builder](https://hub.docker.com/r/forumi0721entwareaarch64build/entware-aarch64-jellyfin-builder/)
![Docker Image Version (tag latest semver)](https://img.shields.io/docker/v/forumi0721entwareaarch64build/entware-aarch64-jellyfin-builder/latest)
![Docker Image Size (tag)](https://img.shields.io/docker/image-size/forumi0721entwareaarch64build/entware-aarch64-jellyfin-builder/latest)
![MicroBadger Layers (tag)](https://img.shields.io/microbadger/layers/forumi0721entwareaarch64build/entware-aarch64-jellyfin-builder/latest)
![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721entwareaarch64build/entware-aarch64-jellyfin-builder)
![Docker Stars](https://img.shields.io/docker/stars/forumi0721entwareaarch64build/entware-aarch64-jellyfin-builder)
#### [entware-armhf-jellyfin-builder](https://hub.docker.com/r/forumi0721entwarearmhfbuild/entware-armhf-jellyfin-builder/)
![Docker Image Version (tag latest semver)](https://img.shields.io/docker/v/forumi0721entwarearmhfbuild/entware-armhf-jellyfin-builder/latest)
![Docker Image Size (tag)](https://img.shields.io/docker/image-size/forumi0721entwarearmhfbuild/entware-armhf-jellyfin-builder/latest)
![MicroBadger Layers (tag)](https://img.shields.io/microbadger/layers/forumi0721entwarearmhfbuild/entware-armhf-jellyfin-builder/latest)
![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721entwarearmhfbuild/entware-armhf-jellyfin-builder)
![Docker Stars](https://img.shields.io/docker/stars/forumi0721entwarearmhfbuild/entware-armhf-jellyfin-builder)



----------------------------------------
#### Description

* Distribution : [Entware](https://github.com/Entware/Entware/)
* Architecture : x64,aarch64,armhf
* Appplication : [Jellyfin](https://jellyfin.github.io/) (Build Image)
    - Jellyfin is a Free Software Media System that puts you in control of managing and streaming your media.



----------------------------------------
#### Run

* Nothing



----------------------------------------
#### Usage

* Nothing



----------------------------------------
#### Docker Options

| Option             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Ports

| Port               | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Volumes

| Volume             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Environment Variables

| ENV                | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


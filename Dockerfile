ARG BUILD_ARCH=x64

FROM forumi0721alpinex64/alpine-x64-dotnet-sdk:latest as jellyfin

LABEL maintainer="forumi0721@gmail.com"

ENV BUILD_ARCH=${BUILD_ARCH}
#ENV DOTNET_RUNTIME=linux-x64

COPY local/. /usr/local/

#RUN ["docker-build-start"]

RUN ["docker-init"]

#RUN ["docker-build-end"]



FROM forumi0721/busybox-${BUILD_ARCH}-base:latest

LABEL maintainer="forumi0721@gmail.com"

COPY --from=jellyfin /output /output

